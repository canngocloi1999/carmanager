create database intern_car;

DROP TABLE IF EXISTS `car_brands`;

CREATE TABLE `car_brands` (
  `id` mediumint NOT NULL AUTO_INCREMENT,
  `name` char(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;



INSERT INTO `car_brands` VALUES (1,'Honda'),(2,'Toyota'),(3,'Mercedes'),(4,'Hyundai'),(5,'Lexus'),(6,'Mazda');


--
-- Table structure for table `cars`
--

DROP TABLE IF EXISTS `cars`;

CREATE TABLE `cars` (
  `id` mediumint NOT NULL AUTO_INCREMENT,
  `name` char(30) NOT NULL,
  `color` char(30) NOT NULL,
  `image` char(255) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `brand_id` mediumint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `brand_id` (`brand_id`),
  CONSTRAINT `cars_ibfk_1` FOREIGN KEY (`brand_id`) REFERENCES `car_brands` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


--
-- Dumping data for table `cars`
--


INSERT INTO `cars` VALUES (1,'Mazda 3','white','/home/canngocloi/Pictures/mazda3.png',50000.00,6),(2,'mazda3','white','/home/canngocloi/Pictures/mazda3.png',40000.00,6),(3,'Honda Civic 2021',' redddd','/home/canngocloi/Pictures/z2600493597592-b2aedfe0f3948d5-5154-2219-1625715558 (1).jpg',35000.00,1),(9,'test','blue','/home/canngocloi/Pictures/mazda3.png',60000.00,2),(13,'test','test','/home/canngocloi/Pictures/mazda3.png',20000.00,3),(18,'test','red','/home/canngocloi/Pictures/mazda3.png',300.00,6),(19,'ssdfs','red','/home/canngocloi/Pictures/mazda3.png',3.30,6),(20,'test','test','/home/canngocloi/Pictures/z2600493597592-b2aedfe0f3948d5-5154-2219-1625715558 (1).jpg',4.00,1);

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `email` char(100) NOT NULL,
  `password` char(20) NOT NULL,
  `name` char(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` char(10) NOT NULL,
  `birth` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `phone` (`phone`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `users`
--


INSERT INTO `users` VALUES (1,'loi@gmail.com','12345678','Cấn NGọc Lợi','0999999999','1999-08-02');

-- Dump completed on 2021-07-26 15:26:34
