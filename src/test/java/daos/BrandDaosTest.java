package daos;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.util.List;
import javax.annotation.processing.Generated;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import models.Brand;

@Generated(value = "org.junit-tools-1.1.0")
public class BrandDaosTest {

	@Before
	public void setUp() throws Exception {

	}

	@After
	public void tearDown() throws Exception {

	}

	private BrandDaos createTestSubject() {
		return new BrandDaos();
	}

//	@MethodRef(name = "getBrandFromRs", signature = "(QResultSet;)QBrand;")
//	@Test
//	public void testGetBrandFromRs() throws Exception {
//		BrandDaos testSubject;
//		ResultSet rs = null;
//		Brand result;
//
//		// default test
//		testSubject = createTestSubject();
//		result = testSubject.getBrandFromRs(rs);
//	}

//	@MethodRef(name = "getAll", signature = "()QList<QBrand;>;")
	@Test
	public void testGetAll() throws Exception {
		BrandDaos testSubject;
		List<Brand> result;

		// default test
		testSubject = createTestSubject();
		result = testSubject.getAll();
		System.out.println(result);
		assertNotNull(result);
	}

//	@MethodRef(name = "save", signature = "(QBrand;)Z")
//	@Test
//	public void testSave() throws Exception {
//		BrandDaos testSubject;
//		Brand brand = new Brand();
//		brand.setId(0);
//		brand.setName("haha");
//		boolean result;
//
//		// default test
//		testSubject = createTestSubject();
//		result = testSubject.save(brand);
//		assertTrue(result);
//	}
//
////	@MethodRef(name = "update", signature = "(QBrand;)Z")
	@Test
	public void testUpdate() throws Exception {
		BrandDaos testSubject;
		Brand brand = new Brand();
		brand.setId(14);
		brand.setName("haha1");
		boolean result;

		// default test
		testSubject = createTestSubject();
		result = testSubject.update(brand);
		assertTrue(result);
	}
//
////	@MethodRef(name = "delete", signature = "(QBrand;)Z")
	@Test
	public void testDelete() throws Exception {
		BrandDaos testSubject;
		Brand brand = new Brand();
		brand.setId(14);
		brand.setName("haha1");
		boolean result;

		// default test
		testSubject = createTestSubject();
		result = testSubject.delete(brand);
		assertTrue(result);
	}
}