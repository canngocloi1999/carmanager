package daos;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.annotation.processing.Generated;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.mysql.cj.exceptions.AssertionFailedException;

import junit.framework.AssertionFailedError;

@Generated(value = "org.junit-tools-1.1.0")
public class ResultsetTest {

	@Before
	public void setUp() throws Exception {

	}

	@After
	public void tearDown() throws Exception {

	}

	private Resultset createTestSubject() {
		return new Resultset();
	}

//	@MethodRef(name = "resultSet", signature = "(QString;)QResultSet;")
	@Test
	public void testResultSet() throws Exception {
		String sql = "select * from cars";
		ResultSet result;

		// default test
		result = Resultset.resultSet(sql);
		System.out.println(result);
		assertNotNull(result);
	}
	
	@Test
	public void testResultSet1() throws Exception {
		String sql = "select * from car_brands";
		ResultSet result;

		// default test
		result = Resultset.resultSet(sql);
		System.out.println(result);
		assertNotNull(result);
	}
	
	@Test(expected = Exception.class)
	public void testResultSet2() throws Exception {
		String sql = "select * from c";
		ResultSet result;

		// default test
		result = Resultset.resultSet(sql);
	}
}