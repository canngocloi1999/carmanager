package util;

import static org.junit.Assert.assertEquals;

import javax.annotation.processing.Generated;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import models.Car;

@Generated(value = "org.junit-tools-1.1.0")
public class sortUtilTest {

	@Before
	public void setUp() throws Exception {

	}

	@After
	public void tearDown() throws Exception {

	}

	public sortUtil createTestSubject() {
		return new sortUtil();
	}

//	@MethodRef(name = "compare", signature = "(QCar;QCar;)I")
	@Test
	// giá xe 1 lớn hơn xe 2 > -1
	public void testCompare() throws Exception {
		sortUtil testSubject;
		Car s1 = new Car();
		s1.setPrice(4000);
		Car s2 = new Car();
		s2.setPrice(3000);
		int result;

		// default test
		testSubject = createTestSubject();
		result = testSubject.compare(s1, s2);
		assertEquals(result, -1);
	}
	
	@Test
	// giá xe 1 < xe 2 => 1
	public void testCompare1() throws Exception {
		sortUtil testSubject;
		Car s1 = new Car();
		s1.setPrice(4000);
		Car s2 = new Car();
		s2.setPrice(5000);
		int result;

		// default test
		testSubject = new sortUtil();
		result = testSubject.compare(s1, s2);
		assertEquals(result, 1);
	}
	
	@Test
	// giá 2 xe bằng nhau => 0
	public void testCompare2() throws Exception {
		sortUtil testSubject;
		Car s1 = new Car();
		s1.setPrice(4000);
		Car s2 = new Car();
		s2.setPrice(4000);
		int result;

		// default test
		testSubject = new sortUtil();
		result = testSubject.compare(s1, s2);
		assertEquals(result, 0);
	}

}
