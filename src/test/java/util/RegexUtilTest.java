package util;

import static org.junit.Assert.*;

import javax.swing.JTextField;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class RegexUtilTest {

	private JTextField txtNull = new JTextField();
	private JTextField txt3Char = new JTextField();
	private JTextField specialChar = new JTextField();
	private JTextField txtprice = new JTextField();

	@Before
	public void setUp() throws Exception {
		txtNull.setText("");
		txt3Char.setText("abc");
		specialChar.setText("ab.");
		txtprice.setText("3000");
	}

	@After
	public void tearDown() throws Exception {
	}

	// nhập kí tự rỗng vào name > false
	@Test
	public void testCheckName() {
		boolean result = RegexUtil.checkName(txtNull);
		assertFalse(result);
	}

	// nhập đúng > true
	@Test
	public void testCheckName1() {
		boolean result = RegexUtil.checkName(txt3Char);
		assertTrue(result);
	}

	// nhập kí tự rỗng vào color > false
	@Test
	public void testCheckColor() {
		boolean result = RegexUtil.checkColor(txtNull);
		assertFalse(result);
	}

	// trường hợp nhập đúng 3 kí tự không chứa kí tự đặc biệt > true
	@Test
	public void testCheckColor1() {
		boolean result = RegexUtil.checkColor(txt3Char);
		assertTrue(result);
	}

	// nhập kí tự đặc biệt vào color > false
	@Test
	public void testCheckColor2() {
		boolean result = RegexUtil.checkColor(specialChar);
		assertFalse(result);
	}

	// nhập kí tự rỗng vào giá > false
	@Test
	public void testCheckPrice() {
		boolean result = RegexUtil.checkPrice(txtNull);
		assertFalse(result);
	}
	
	//nhập chữ vào giá > false
	@Test
	public void testCheckPrice1() {
		boolean result = RegexUtil.checkPrice(txt3Char);
		assertFalse(result);
	}
	
	//nhập số vào giá > true
	@Test
	public void testCheckPrice2() {
		boolean result = RegexUtil.checkPrice(txtprice);
		assertTrue(result);
	}

//	@Test
//	public void testCheckImage() {
//		fail("Not yet implemented");
//	}

}
