package util;

import static org.junit.Assert.assertNotNull;

import java.sql.Connection;

import javax.annotation.processing.Generated;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

@Generated(value = "org.junit-tools-1.1.0")
public class DbUtilTest {

	@Before
	public void setUp() throws Exception {

	}

	@After
	public void tearDown() throws Exception {

	}

//	private DbUtil createTestSubject() {
//		return new DbUtil();
//	}

//	@MethodRef(name = "getConnection", signature = "()QConnection;")
	@Test
	public void testGetConnection() throws Exception {
		Connection result;

		// default test
		result = DbUtil.getConnection();
		assertNotNull(result);
	}

////	@MethodRef(name = "closeConnection", signature = "()V")
//	@Test
//	public void testCloseConnection() throws Exception {
//
//		// default test
//		DbUtil.closeConnection();
//	}
}