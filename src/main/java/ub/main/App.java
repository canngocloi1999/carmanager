package ub.main;

import daos.BrandDaos;
import daos.CarDaos;
import daos.IDaos;
import java.awt.Color;
import java.awt.Graphics;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import org.apache.logging.log4j.LogManager;
import models.Brand;
import models.Car;
import util.ExportJsonUtil;
import util.RegexUtil;
import util.sortUtil;
import util.ThreadUtil;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author canngocloi
 */
public class App extends javax.swing.JFrame {

    /**
     * Creates new form App
     */
    private DefaultTableModel model;
    private int vitri;
    private static List<Car> listCar = null;
    private List<Brand> listBrand = null;
    private String path;
    private boolean statusAccept;
    private boolean StatusSort = true;
    private static final org.apache.logging.log4j.Logger logger = LogManager.getLogger(App.class);
    private IDaos<Car> carDao = new CarDaos();
    private IDaos<Brand> brandDao = new BrandDaos();

    public App() {
        initComponents();
        vitri = 0;
        FillToTable();
        fillToCombobox();
        showDetail(vitri);
        status(false);
    }

    // đổ dữ liệu từ list vào bảng
    private synchronized void FillToTable() {
        model = (DefaultTableModel) tblCar.getModel();
        model.setRowCount(0);
        try {
            listCar = (List<Car>) carDao.getAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (listCar == null) {
            System.out.println("data trong");
        } else {
            int stt = 0;
            for (Car car : listCar) {
                stt++;
                Object row[] = new Object[]{stt, car.getName(), car.getColor(), car.getPrice(), car.getBrand().getName(), car.getImage()};
                model.addRow(row);
            }
        }
    }

    // dua du lieu vao combobox
    private void fillToCombobox() {
        try {
            listBrand = (List<Brand>) brandDao.getAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (listBrand == null) {
            System.out.println("danh sach brand trong");
        } else {
            String[] arrayBrands = new String[listBrand.size()];
            for (int i = 0; i < listBrand.size(); i++) {
                arrayBrands[i] = listBrand.get(i).getName();
            }
            DefaultComboBoxModel<String> cbnModel = new DefaultComboBoxModel<String>(arrayBrands);
            Cbn_Brand.setModel(cbnModel);
        }
    }

    // hien chi thiet xe len cac truong
    private void showDetail(int vitri) {
        txtCarName.setText(tblCar.getValueAt(vitri, 1).toString());
        txtColor.setText(tblCar.getValueAt(vitri, 2).toString());
        txtPrice.setText(tblCar.getValueAt(vitri, 3).toString());
        Cbn_Brand.setSelectedItem(tblCar.getValueAt(vitri, 4).toString());
        path = tblCar.getValueAt(vitri, 5).toString();
        //hiển thị ảnh lên label
        jLabel_picture.setIcon(new ImageIcon(path));
        tblCar.setRowSelectionInterval(vitri, vitri);
    }

    //thay đổi trạng thái hoạt động của textfield, button, combobox trong giao dien
    // true mở các textfield, combobox, và các button để người dùng thực hiện thay đổi dữ liệu
    // false đóng lại để người dùng chỉ xem mà ko thể thay đổi dữ liệu dc hiển thị
    @SuppressWarnings("serial")
	private void status(boolean enable) {
        txtCarName.setEditable(enable);
        txtColor.setEditable(enable);
        txtPrice.setEditable(enable);
        Cbn_Brand.setEnabled(enable);
        Cbn_Brand.setRenderer(new DefaultListCellRenderer() {
            @Override
            public void paint(Graphics g) {
                setForeground(Color.BLACK);
                super.paint(g);
            }
        });
        btnCreate.setEnabled(!enable);
        btnEdit.setEnabled(!enable);
        btnDelete.setEnabled(!enable);
        btnFirst.setEnabled(!enable);
        btnPrev.setEnabled(!enable);
        btnNext.setEnabled(!enable);
        btnLast.setEnabled(!enable);
        Btn_Browse.setEnabled(enable);
        btnAccept.setEnabled(enable);
        btnCancel.setEnabled(enable);
        btnBands.setEnabled(!enable);
    }

    // clear du lieu trong cac truong
    private void clear() {
        txtCarName.setText("");
        txtColor.setText("");
        txtPrice.setText("");
        path = "";
        jLabel_picture.setIcon(null);

    }

    // get car from showdetail
    private Car getModel() {
    	int brandSelected = Cbn_Brand.getSelectedIndex();
    	Brand brand = listBrand.get(brandSelected);
    	
        Car car = new Car();
        car.setId(listCar.get(vitri).getId());
        car.setName(txtCarName.getText());
        car.setColor(txtColor.getText());
        car.setPrice(Double.parseDouble(txtPrice.getText()));
        car.setImage(path);
        car.setBrand(brand);
        return car;
    }

    //validate intput
    private boolean validateInput() {
        if (RegexUtil.checkName(txtCarName)
                && RegexUtil.checkColor(txtColor)
                && RegexUtil.checkPrice(txtPrice)
                && RegexUtil.checkImage(path, panImage)) {
            return true;
        } else {
            return false;
        }
    }

    // method create new car
    private void createCar() {
        Car car = getModel();
        try {
            boolean addCar = carDao.save(car);
            if (addCar) {
                vitri = listCar.size();
                System.out.println("add successfully");
                logger.info("CREATE NEW CAR NAME: "+car.getName());
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("ERROR CREATE CAR");
        }
    }

    // method update Car
    private void updateCar() {
        Car car = getModel();
        try {
            boolean updated = carDao.update(car);
            if (updated) {
                System.out.println("update successfully");
                logger.info("UPDATE CAR ID: "+car.getId());
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("ERROR UPDATE CAR");
        }
    }

    // method delete car
    private void deleteCar() {
        int Selected = JOptionPane.showConfirmDialog(this, "Do you agree to delete this data?");
        if (Selected == 0) {
            Car car = getModel();
            System.out.println(car.getId());
            try {
                boolean deleted = carDao.delete(car);
                if (deleted) {
                    System.out.println("delete successfully");
                    logger.info("DELETE CAR ID: "+car.getId());
                    FillToTable();
                    vitri--;
                    showDetail(vitri);
                }
            } catch (Exception e) {
                e.printStackTrace();
                logger.error("ERROR DELETE CAR");
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tblCar = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtCarName = new javax.swing.JTextField();
        txtColor = new javax.swing.JTextField();
        txtPrice = new javax.swing.JTextField();
        btnCreate = new javax.swing.JButton();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnAccept = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        btnFirst = new javax.swing.JButton();
        btnPrev = new javax.swing.JButton();
        btnNext = new javax.swing.JButton();
        btnLast = new javax.swing.JButton();
        Cbn_Brand = new javax.swing.JComboBox<>();
        panImage = new javax.swing.JPanel();
        jLabel_picture = new javax.swing.JLabel();
        Btn_Browse = new javax.swing.JButton();
        btnBands = new javax.swing.JButton();
        btnExport = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("CAR MANAGEMENT");
        setResizable(false);

        tblCar.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "STT", "Car name", "Color", "Price", "Brand", "Image"
            }
        ));
        tblCar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblCarMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblCar);

        jLabel1.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        jLabel1.setText("Car Name:");

        jLabel2.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        jLabel2.setText("Color:");

        jLabel3.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        jLabel3.setText("Price:");

        jLabel4.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        jLabel4.setText("Brand:");

        txtCarName.setName("Car Name"); // NOI18N
        txtCarName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCarNameActionPerformed(evt);
            }
        });

        txtColor.setName("Color"); // NOI18N
        txtColor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtColorActionPerformed(evt);
            }
        });

        txtPrice.setName("Price"); // NOI18N

        btnCreate.setText("Create");
        btnCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCreateActionPerformed(evt);
            }
        });

        btnEdit.setText("Edit");
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });

        btnDelete.setText("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        btnAccept.setText("accept");
        btnAccept.setEnabled(false);
        btnAccept.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAcceptActionPerformed(evt);
            }
        });

        btnCancel.setText("Cancel");
        btnCancel.setEnabled(false);
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        btnFirst.setBackground(new java.awt.Color(102, 89, 230));
        btnFirst.setText("<<");
        btnFirst.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFirstActionPerformed(evt);
            }
        });

        btnPrev.setBackground(new java.awt.Color(102, 89, 230));
        btnPrev.setText("<");
        btnPrev.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrevActionPerformed(evt);
            }
        });

        btnNext.setBackground(new java.awt.Color(102, 89, 230));
        btnNext.setText(">");
        btnNext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNextActionPerformed(evt);
            }
        });

        btnLast.setBackground(new java.awt.Color(102, 89, 230));
        btnLast.setText(">>");
        btnLast.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLastActionPerformed(evt);
            }
        });

        Cbn_Brand.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Cbn_BrandActionPerformed(evt);
            }
        });

        panImage.setBackground(new java.awt.Color(108, 122, 137));

        javax.swing.GroupLayout panImageLayout = new javax.swing.GroupLayout(panImage);
        panImage.setLayout(panImageLayout);
        panImageLayout.setHorizontalGroup(
            panImageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel_picture, javax.swing.GroupLayout.DEFAULT_SIZE, 299, Short.MAX_VALUE)
        );
        panImageLayout.setVerticalGroup(
            panImageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel_picture, javax.swing.GroupLayout.DEFAULT_SIZE, 283, Short.MAX_VALUE)
        );

        Btn_Browse.setText("Browse");
        Btn_Browse.setEnabled(false);
        Btn_Browse.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Btn_BrowseActionPerformed(evt);
            }
        });

        btnBands.setText("Brand mag");
        btnBands.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBandsActionPerformed(evt);
            }
        });

        btnExport.setText("Export");
        btnExport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExportActionPerformed(evt);
            }
        });

        jButton1.setText("Highest price");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(354, 354, 354)
                        .addComponent(btnFirst, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(31, 31, 31)
                        .addComponent(btnPrev, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(60, 60, 60)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(Btn_Browse)
                            .addComponent(panImage, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4)
                    .addComponent(btnCreate)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addComponent(btnNext, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(txtCarName)
                                .addComponent(txtColor)
                                .addComponent(txtPrice, javax.swing.GroupLayout.DEFAULT_SIZE, 341, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(Cbn_Brand, javax.swing.GroupLayout.PREFERRED_SIZE, 265, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnBands)))
                        .addContainerGap(34, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnLast, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButton1)
                                .addGap(18, 18, 18)
                                .addComponent(btnExport))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(43, 43, 43)
                                .addComponent(btnDelete)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 88, Short.MAX_VALUE)
                                .addComponent(btnAccept)
                                .addGap(18, 18, 18)
                                .addComponent(btnCancel)))
                        .addGap(23, 23, 23))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(txtCarName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(txtColor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(txtPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(Cbn_Brand, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnBands))
                        .addGap(61, 61, 61)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnCreate)
                            .addComponent(btnEdit)
                            .addComponent(btnDelete)
                            .addComponent(btnAccept)
                            .addComponent(btnCancel)))
                    .addComponent(panImage, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(Btn_Browse)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 58, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnPrev)
                    .addComponent(btnNext)
                    .addComponent(btnLast)
                    .addComponent(btnFirst)
                    .addComponent(btnExport)
                    .addComponent(jButton1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void txtColorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtColorActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtColorActionPerformed

    private void btnCreateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCreateActionPerformed
        // TODO add your handling code here:
        clear();
        status(true);
        statusAccept = true;

    }//GEN-LAST:event_btnCreateActionPerformed

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
        // TODO add your handling code here:
        status(true);
        statusAccept = false;
    }//GEN-LAST:event_btnEditActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        // TODO add your handling code here:
        showDetail(vitri);
        status(false);
    }//GEN-LAST:event_btnCancelActionPerformed

    private void btnFirstActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFirstActionPerformed
        // TODO add your handling code here:
        vitri = 0;
        showDetail(vitri);

    }//GEN-LAST:event_btnFirstActionPerformed

    private void Btn_BrowseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Btn_BrowseActionPerformed
        // TODO add your handling code here:
        JFileChooser filec = new JFileChooser();
        filec.setCurrentDirectory(new File(System.getProperty("user.home")));
        //file extension
        FileNameExtensionFilter fileFilter = new FileNameExtensionFilter(".Images", "jpg", "png", "gif");
        filec.addChoosableFileFilter(fileFilter);

        int fileState = filec.showSaveDialog(null);
        // nếu người dùng chọn file
        if (fileState == JFileChooser.APPROVE_OPTION) {
            File selectedFile = filec.getSelectedFile();
            path = selectedFile.getAbsolutePath();
            //hiển thị ảnh lên label
            jLabel_picture.setIcon(new ImageIcon(path));
        } // neu nguoi dung cancel
        else if (fileState == JFileChooser.CANCEL_OPTION) {
            System.out.println("No image selected");
        }
    }//GEN-LAST:event_Btn_BrowseActionPerformed

    private void Cbn_BrandActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Cbn_BrandActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_Cbn_BrandActionPerformed

    private void tblCarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblCarMouseClicked
        // TODO add your handling code here:
        vitri = tblCar.getSelectedRow();
        showDetail(vitri);
    }//GEN-LAST:event_tblCarMouseClicked

    private void btnAcceptActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAcceptActionPerformed
        // TODO add your handling code here:
        if (validateInput()) {
            if (statusAccept) {
                createCar();
            } else {
                updateCar();
            }
            FillToTable();
            showDetail(vitri);
            status(false);
        }
    }//GEN-LAST:event_btnAcceptActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        // TODO add your handling code here:
        deleteCar();
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnLastActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLastActionPerformed
        // TODO add your handling code here:
        vitri = listCar.size() - 1;
        showDetail(vitri);
    }//GEN-LAST:event_btnLastActionPerformed

    private void btnPrevActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrevActionPerformed
        // TODO add your handling code here:
        if (vitri > 0) {
            vitri--;
            showDetail(vitri);
        }
    }//GEN-LAST:event_btnPrevActionPerformed

    private void btnNextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNextActionPerformed
        // TODO add your handling code here:
        if (vitri < (listCar.size() - 1)) {
            vitri++;
            showDetail(vitri);
        }
    }//GEN-LAST:event_btnNextActionPerformed

    private void btnBandsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBandsActionPerformed
        // TODO add your handling code here:
        new BrandManagement().setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_btnBandsActionPerformed

    private void btnExportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExportActionPerformed
        // TODO add your handling code here:
//        ObjectOutput.objectOut(listBrand);
//        Car car = listCar.get(0);
        try {
            ExportJsonUtil.ObjectToJson(listCar);
        } catch (IOException ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnExportActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        // sắp xếp danh sách car theo giá ASC
        // StatusSort mặc định = true
        // user nhấn button sắp xếp table car theo giá cao nhất sẽ thực hiện sắp xếp lại listcar
        if (StatusSort) {
            Collections.sort(listCar, new sortUtil());
            //fill danh sách Car lên jtable
            model = (DefaultTableModel) tblCar.getModel();
            model.setRowCount(0);
            int stt = 0;
            for (Car car : listCar) {
                stt++;
                Object row[] = new Object[]{stt, car.getName(), car.getColor(), car.getPrice(), car.getBrand().getName(), car.getImage()};
                model.addRow(row);
            }
            vitri = 0;
            showDetail(vitri);
            // hiển thị lại danh sách listcar ban đầu trước khi bị sắp xếp
        } else {
            FillToTable();
            showDetail(vitri);
        }
        StatusSort = !StatusSort;
    }//GEN-LAST:event_jButton1ActionPerformed

    private void txtCarNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCarNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCarNameActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(App.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(App.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(App.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(App.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new App().setVisible(true);
            }
        });
        ThreadUtil t1 = new ThreadUtil();
        t1.start();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Btn_Browse;
    private javax.swing.JComboBox<String> Cbn_Brand;
    private javax.swing.JButton btnAccept;
    private javax.swing.JButton btnBands;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnCreate;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnExport;
    private javax.swing.JButton btnFirst;
    private javax.swing.JButton btnLast;
    private javax.swing.JButton btnNext;
    private javax.swing.JButton btnPrev;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel_picture;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel panImage;
    private javax.swing.JTable tblCar;
    private javax.swing.JTextField txtCarName;
    private javax.swing.JTextField txtColor;
    private javax.swing.JTextField txtPrice;
    // End of variables declaration//GEN-END:variables
}
