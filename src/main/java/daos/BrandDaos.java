/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daos;

import util.DbUtil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import models.Brand;

/**
 *
 * @author canngocloi
 */

public class BrandDaos extends Resultset implements IDaos<Brand> {

	// nhận dữ liệu từ resulset chuyển thành Brand rồi trả về
	// haha
	@Override
	public Brand getModelFromRs(ResultSet rs) throws SQLException {
		int id = rs.getInt(1);
		String name = rs.getString(2);

		Brand brand = new Brand();
		brand.setId(id);
		brand.setName(name);
		return brand;
	}

	// lấy tất cả Brand trong db
	@Override
	public List<Brand> getAll() throws Exception {
		List<Brand> listBrand = new ArrayList<>();
		String sql = "select * from car_brands";
			ResultSet rs = resultSet(sql);
			while (rs.next()) {
				Brand brand = getModelFromRs(rs);
				listBrand.add(brand);
			}
			DbUtil.closeConnection();
			return listBrand;
	}

	// thêm mới dữ liệu vào bảng car_brand
	@Override
	public boolean save(Brand brand) {
		Connection cn = DbUtil.getConnection();
		String sql = "insert into car_brands (name) values (?)";
		try {
			PreparedStatement pstm = cn.prepareStatement(sql);
			pstm.setString(1, brand.getName());
			pstm.executeUpdate();
			pstm.close();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	// cập nhật brand trong db
	@Override
	public boolean update(Brand brand) {
		Connection cn = DbUtil.getConnection();
		String sql = "update car_brands set name = ? where id = ?";
		try {
			PreparedStatement pstm = cn.prepareStatement(sql);
			pstm.setString(1, brand.getName());
			pstm.setInt(2, brand.getId());
			pstm.executeUpdate();
			pstm.close();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	// xóa car trong db
	@Override
	public boolean delete(Brand brand) {
		Connection cn = DbUtil.getConnection();
		String sql = "delete from car_brands where id = ?";
		try {
			PreparedStatement pstm = cn.prepareStatement(sql);
			pstm.setInt(1, brand.getId());
			pstm.executeUpdate();
			pstm.close();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
}
