package daos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

public interface IDaos<T> {
	
	T getModelFromRs(ResultSet rs) throws SQLException;
	
	Collection<T> getAll() throws Exception;
	
	boolean save(T t);
	
	boolean update(T t);
	
	boolean delete(T t);

}
