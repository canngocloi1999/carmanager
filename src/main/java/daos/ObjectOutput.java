/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daos;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;
import models.Brand;

/**
 *
 * @author canngocloi
 */
public class ObjectOutput {

    private final static String FILE_URL = "/home/canngocloi/Pictures/data.txt";

    public static void objectOut(List<Brand> brand) {

        //Ghi Object vao file
        try {   // dat try cacth de tranh ngoai le khi tao va viet File
            FileOutputStream f = new FileOutputStream(FILE_URL); // tao file f tro den student.dat
            ObjectOutputStream oStream = new ObjectOutputStream(f); // dung de ghi theo Object vao file f
            oStream.writeObject(brand); // ghi MyStudent theo kieu Object vao file
            oStream.close();
            System.out.println("ghi thanh cong");
        } catch (IOException e) {
            System.out.println("Error Write file");
        }

        // doc Object tu file
        List<Brand> ms = null;

        try {   // dat try cacth de tranh ngoai le khi tao va doc File
            FileInputStream f = new FileInputStream(FILE_URL); // tao file f tro den student.dat
            ObjectInputStream inStream = new ObjectInputStream(f);  // dung de doc theo Object vao file f

            // dung inStream doc theo Object, ep kieu tra ve la MyStudent
            ms = (List<Brand>) inStream.readObject();
            for (Brand m : ms) {
                System.out.println(m.getName());
            }
            inStream.close();
        } catch (ClassNotFoundException e) {
            System.out.println("Class not found");
        } catch (IOException e) {
            System.out.println("Error Read file");
        }

    }
}
