package daos;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import util.DbUtil;

public class Resultset {

	public static ResultSet resultSet(String sql) throws Exception {
		Connection cn = DbUtil.getConnection();
		try {
			Statement st = cn.createStatement();
			ResultSet rs = st.executeQuery(sql);
			return rs;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
	}

}
