/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daos;

import util.DbUtil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import models.Brand;
import models.Car;

/**
 *
 * @author canngocloi
 */
public class CarDaos extends Resultset implements IDaos<Car> {

	// lấy đối tượng car từ resultset
	@Override
	public Car getModelFromRs(ResultSet rs) throws SQLException {
		int id = rs.getInt(1);
		String name = rs.getString(2);
		String color = rs.getString(3);
		double price = rs.getDouble(5);
		String image = rs.getString(4);
		String brandName = rs.getString(6);
		int brandId = rs.getInt(7);
		
		Brand brand = new Brand(brandId, brandName);
		
		Car car = new Car();
		car.setId(id);
		car.setName(name);
		car.setColor(color);
		car.setPrice(price);
		car.setBrand(brand);
		car.setImage(image);

		return car;
	}

	// lấy toàn bộ dữ liệu từ bảng cars trong db
	@Override
	public List<Car> getAll() throws Exception {
		List<Car> listCars = new ArrayList<>();
		String sql = "select cars.id, cars.name, color, image, price, "
				+ "car_brands.name, car_brands.id from cars inner join car_brands " 
				+ "on cars.brand_id = car_brands.id;";
		ResultSet rs = resultSet(sql);
		while (rs.next()) {
			Car car = getModelFromRs(rs);
			listCars.add(car);
		}
		rs.close();
		DbUtil.closeConnection();
		return listCars;
	}

	// thêm mới dữ liệu vào bảng cars
	@Override
	public boolean save(Car car) {
		Connection cn = DbUtil.getConnection();
		String sql = "insert into cars (name, color, price, image, brand_id) values (?,?,?,?,?)";
		try {
			PreparedStatement pstm = cn.prepareStatement(sql);
			pstm.setString(1, car.getName());
			pstm.setString(2, car.getColor());
			pstm.setDouble(3, car.getPrice());
			pstm.setString(4, car.getImage());
			pstm.setInt(5, car.getBrand().getId());
			pstm.executeUpdate();
			pstm.close();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	// cập nhật car trong db
	@Override
	public boolean update(Car car) {
		Connection cn = DbUtil.getConnection();
		String sql = "update cars set name = ?, color = ?, price = ?, image = ?, brand_id = ? where id = ?";
		try {
			PreparedStatement pstm = cn.prepareStatement(sql);
			pstm.setString(1, car.getName());
			pstm.setString(2, car.getColor());
			pstm.setDouble(3, car.getPrice());
			pstm.setString(4, car.getImage());
			pstm.setInt(5, car.getBrand().getId());
			pstm.setInt(6, car.getId());
			pstm.executeUpdate();
			pstm.close();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	// xóa car trong db
	@Override
	public boolean delete(Car car) {
		Connection cn = DbUtil.getConnection();
		String sql = "delete from cars where id = ?";
		try {
			PreparedStatement pstm = cn.prepareStatement(sql);
			pstm.setInt(1, car.getId());
			pstm.executeUpdate();
			pstm.close();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

//	// find all car by id brand
//	public static List<Car> findCarByIdBrand(int idBrand) throws DbException {
//		Connection cn = DbUtil.getConnection();
//		List<Car> listCars = new ArrayList<>();
//		String sql = "select cars.id, cars.name, color, image, price,"
//				+ " car_brands.name from cars inner join car_brands" + " on cars.brand_id = car_brands.id "
//				+ "where brand_id = ?;";
//		try {
//			PreparedStatement st = cn.prepareStatement(sql);
//			st.setInt(1, idBrand);
//			ResultSet rs = st.executeQuery();
//			while (rs.next()) {
//				Car car = getCarFromRs(rs);
//				listCars.add(car);
//			}
//			rs.close();
//			DbUtil.closeConnection();
//			return listCars;
//		} catch (SQLException e) {
//			throw new DbException(e);
//		}
//	}

}
