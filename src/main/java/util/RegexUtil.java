/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.awt.Color;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author canngocloi
 */
public class RegexUtil {

	// check dữ liệu được nhập vào các textName
    public static boolean checkName(JTextField txtName) {
        txtName.setBackground(Color.white);
        String name = txtName.getText();
        // nhận tất cả kí tự có độ dài từ 3 đến 50
        String rgx = ".{3,50}";
        if (name.matches(rgx)) {
            return true;
        } else {
        	//dữ liệu nhập sai textfield sẽ chuyển sang màu hồng vào báo lỗi
            txtName.setBackground(Color.PINK);
            JOptionPane.showMessageDialog(txtName.getRootPane(), txtName.getName() + " phải từ 3 đến 50 kí tự!");
            return false;
        }
    }

    //check textfield color
    public static boolean checkColor(JTextField txtColor) {
        txtColor.setBackground(Color.white);
        String name = txtColor.getText();
        //chỉ nhận kí tự chữ cái có độ dài từ 3 đến 50
        String rgx = "[a-zA-z]{3,50}";
        if (name.matches(rgx)) {
            return true;
        } else {
            txtColor.setBackground(Color.PINK);
            JOptionPane.showMessageDialog(txtColor.getRootPane(), txtColor.getName() + " phải từ 3 đến 50 kí tự và không chứ chữ số!");
            return false;
        }
    }

    //check giá khi nhập vào textfield
    public static boolean checkPrice(JTextField txt) {
        txt.setBackground(Color.white);
        String name = txt.getText();
        double price;
        //kiểm tra dữ liệu nhập vào đúng kiểu dữ liệu double không
        try {
        	price = Double.parseDouble(name);
            return true;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(txt.getRootPane(), txt.getName() + " phải là chữ số!");
            return false;
        }

    }

    // check hình ảnh
    public static boolean checkImage(String image, JPanel pan) {
    	//kiểm tra xem người dùng đã chọn ảnh chưa
        if (!image.isEmpty()) {
            return true;
        } else {
            JOptionPane.showMessageDialog(pan.getRootPane(), " Image not found!");
            return false;
        }

    }

}
