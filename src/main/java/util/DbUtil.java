/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author canngocloi
 */
public class DbUtil {

	private static Connection connection;
	private static String DB_URL = "jdbc:mysql://localhost:3306/intern_car";
	private static String USER_NAME = "root";
	private static String PASSWORD = "loicn2899";

	public synchronized static Connection getConnection(){
		if (connection != null) {
			return connection;
		} else {
			try {
//            Class.forName("com.mysql.jdbc.Driver");
				connection = DriverManager.getConnection(DB_URL, USER_NAME, PASSWORD);
				System.out.println("connect successfully!");
				return connection;
			} catch (Exception e) {
				System.out.println("connect failure!");
				e.printStackTrace();
			}

		}
		return connection;
	}

	// ngat ket noi db
	public synchronized static void closeConnection(){
		if (connection != null) {
			try {
				connection.close();
				System.out.println("đóng kết nối");
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				connection = null;
			}
		}
	}

}
