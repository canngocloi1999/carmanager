/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import daos.CarDaos;
import daos.IDaos;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.LogManager;

import models.Car;

/**
 *
 * @author canngocloi
 */
public class ThreadUtil extends Thread {

	private List<Car> listCar;
	private static final org.apache.logging.log4j.Logger logger = LogManager.getLogger(ThreadUtil.class);
	private IDaos<Car> carDao = new CarDaos();

	public ThreadUtil() {

	}

	public List<Car> getListCar() {
		return listCar;
	}

	public void setListCar(List<Car> listCar) {
		this.listCar = listCar;
	}

	@Override
	public void run() {
		// luống sẽ lặp lại đến khi đóng chương trình
		for (int i = 0; true; i++) {
			try {
				// đưa luồng vào chế độ sleep, sau 1 giờ lại thực hiện xuất dữ liệu 1 lần
				Thread.sleep(3600000);
				List<Car> list = null;
				try {
					// lấy tất cả Car trong db gán vào list
					list = (List<Car>) carDao.getAll();
				} catch (Exception ex) {
					ex.printStackTrace();
					logger.error("ERROR: get all car in db");
				}
				// sắp xếp danh sách car vừa lấy theo giá ASC
				Collections.sort(list, new sortUtil());
				// lấy gia danh sách 5 xe có giá cao nhất
				if (list.size() <= 5) {
					this.listCar = list;
				} else {
					List<Car> l = new ArrayList<>();
					for (int j = 0; j < 5; j++) {
						l.add(list.get(j));
					}
					this.listCar = l;
				}
				System.out.println("xuat lan: " + i);
				// ghi danh sách xe ra json
				try {
					ExportJsonUtil.ObjectToJson(this.listCar);
					logger.info("Export file json");
				} catch (IOException ex) {
					ex.printStackTrace();
					logger.error("ERROR: export file json");
				}

			} catch (InterruptedException e) {
				e.printStackTrace();
				logger.error("ERROR: thread");
				break;
			}
		}
	}
}
