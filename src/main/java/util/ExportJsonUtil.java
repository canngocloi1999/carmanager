/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import models.Car;

/**
 *
 * @author canngocloi
 */
public class ExportJsonUtil {
	private static String FILE_URL = "car" + LocalDateTime.now() + ".json";
	private static File PATH = new File(FILE_URL).getAbsoluteFile();

	public static void ObjectToJson(List<Car> listCar) throws IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		ObjectWriter objectWriter = objectMapper.writer(new DefaultPrettyPrinter());
		objectWriter.writeValue(PATH, listCar);
//		List<Car> c = objectMapper.readValue(PATH, List.class);
//		System.out.println(c);
		System.out.println("Export thanh cong " + PATH);
	}

}
