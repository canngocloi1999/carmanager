package util;

import java.util.Comparator;

import models.Car;

public class sortUtil implements Comparator<Car> {

	@Override
	public int compare(Car s1, Car s2) {
		if (s1.getPrice() == s2.getPrice())
			return 0;
		else if (s1.getPrice() > s2.getPrice())
			return -1;
		else
			return 1;

	}

}
